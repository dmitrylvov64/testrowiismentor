﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTask
{
    public class QA : EmployeeBase
    {
        private readonly DateTime DateOfEmployment;

        public QA(string lastName, string firstName, string middleName, DateTime dateOfEmployment) : base(lastName, firstName, middleName)
        {       
            DateOfEmployment = dateOfEmployment;           
        }

        public override void Print()
        {
            base.Print();
            if (IsMentor())
            {                
                Console.WriteLine("Является наставником");
            }
        }

        public bool IsMentor() // Метод не приватный, т.к. будут тесты на этот метод
        {
            return DateOfEmployment.AddYears(1) <= DateTime.Now;
        }
    }
}
