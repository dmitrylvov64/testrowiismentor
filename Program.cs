﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeTask
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Укажите тип сотрудника: 1 - Developer, 2 - DevOps, 3 - QA");

            var employeeType = Console.ReadLine();

            if (!(employeeType == "1" || employeeType == "2" || employeeType == "3"))
            {
                Console.WriteLine("Указан неизвестный тип сотрудника");
                Console.ReadLine();

                return;
            }

            Console.WriteLine("Укажите фамилию");
            var f = Console.ReadLine();

            Console.WriteLine("Укажите имя");
            var i = Console.ReadLine();

            Console.WriteLine("Укажите отчество");
            var o = Console.ReadLine();


            if (employeeType == "1")
            {
                Developer developer = new Developer(f, i, o);
                developer.Print();
            }

            else if (employeeType == "2")
            {
                DevOps devOps = new DevOps(f, i, o);
                devOps.Print();
            }

            else if (employeeType == "3")
            {
                Console.WriteLine("Укажите дату приема на работу в формате dd.mm.yyyy");
                var dStr = Console.ReadLine();
                var isDate = DateTime.TryParse(dStr, out DateTime d);
                if (!isDate)
                {
                    Console.WriteLine("Неверная дата");
                    Console.ReadLine();
                    return;                    
                }
                QA qAMentor = new QA(f, i, o, d);
                qAMentor.Print();
            }

            Console.ReadLine();
        }
    }
}

