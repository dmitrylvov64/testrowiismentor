﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EmployeeTask
{
    /// <summary>
    /// Базовый класс для сотрудника
    /// </summary>
    public abstract class EmployeeBase
    {

        public string LastName
        {
            get { return lastName; }
            set { lastName = value ?? noData; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value ?? noData; }
        }

        public string MiddleName
        {
            get { return middleName; }
            set { middleName = value ?? noData; }
        }


        private string lastName;

        private string firstName;

        private string middleName;

        private readonly string noData = "{Нет данных}";

        public EmployeeBase(string lastName, string firstName, string middleName)
        {
            LastName = lastName;
            FirstName = firstName;
            MiddleName = middleName;

        }


        public virtual void Print()
        {
            Console.WriteLine($"\n\nСотрудник\nФамилия: {LastName}\nИмя: {FirstName}\nОтчество: {MiddleName}");
        }
    }
}
