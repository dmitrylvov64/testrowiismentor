﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using EmployeeTask;
using System.Collections.Generic;

namespace EmployeeTests
{
    [TestClass]
    public class EmployeeTaskTests
    {
        private static IEnumerable<object[]> GetTestStrings()
        {
            yield return new object[] { "Петров1", "Яков1", "Валентинович1", DateTime.Now.AddYears(-1).AddMonths(0).AddDays(0) };
            yield return new object[] { "Петров2", "Яков2", "Валентинович2", DateTime.Now.AddYears(-1).AddMonths(0).AddDays(-1) };
            yield return new object[] { "Петров3", "Яков3", "Валентинович3", DateTime.Now.AddYears(-1).AddMonths(-6).AddDays(0) };
        }

        private static IEnumerable<object[]> GetTestStrings2()
        {
            yield return new object[] { "Петров4", "Яков4", "Валентинович4", DateTime.Now.AddYears(-1).AddMonths(0).AddDays(1) };
            yield return new object[] { "Петров5", "Яков5", "Валентинович5", DateTime.Now.AddYears(-1).AddMonths(0).AddDays(1) };
            yield return new object[] { "Петров6", "Яков6", "Валентинович6", DateTime.Now.AddYears(0).AddMonths(-6).AddDays(0) };
        }

        [DataTestMethod]
        [DynamicData(nameof(GetTestStrings), DynamicDataSourceType.Method)]
        public void TestCheckIsMentor(string lastName, string firstName, string middleName, DateTime date)
        {
            QA qaMentor = new QA(lastName, firstName, middleName, date);

            Assert.IsTrue(qaMentor.IsMentor());
        }

        [DataTestMethod]
        [DynamicData(nameof(GetTestStrings2), DynamicDataSourceType.Method)]
        public void TestCheckIsNoMentor(string lastName, string firstName, string middleName, DateTime date)
        {
            QA qaNoMentor = new QA(lastName, firstName, middleName, date);

            Assert.IsFalse(qaNoMentor.IsMentor());
        }
    }
}
